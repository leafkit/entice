<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

 	<section class="blog">

 		<div class="container">

	 			<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="3"]'); ?>

	</section>

<?php get_footer(); ?>
