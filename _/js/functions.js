// Browser detection for when you get desparate. A measure of last resort.
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }


// remap jQuery to $
(function ($) {

	/* trigger when page is ready */
	$(document).ready(function (){

		if ($('.slides.foreground').length) {

			$('.slides.foreground').slick({
				dots: false,
				prevArrow: false,
				nextArrow: false,
				autoplay: false,
				autoplaySpeed: 5000,
				asNavFor: '.slides.background'
			});

			$('.slides.background').slick({
				dots: false,
				prevArrow: false,
				nextArrow: false,
				autoplay: false,
				autoplaySpeed: 5000,
				asNavFor: '.slides.foreground'
			});

		}

		 var afterChange = function(slider,i) {
		    var slideHeight = jQuery(slider.$slides[i] ).height();
		    jQuery(slider.$slider ).height( slideHeight);
		  };

		if ($('.testimonials .slides').length) {

			$('.testimonials .slides').slick({
				dots: false,
				arrows: true,
				prevArrow: '<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
				nextArrow: '<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>',
				autoplay: true,
				autoplaySpeed: 5000,
				dots: true,
				dotsClass: 'testimonial-dots',
				adaptiveHeight: true
			});
		}

		 $('.slider-for .slides').slick({
			    slidesToShow: 1,
			    slidesToScroll: 1,
			    asNavFor: '.slider-nav .slides',
			    prevArrow: '<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
		    	nextArrow: '<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>',
		    	onAfterChange: afterChange,
		    	adaptiveHeight: true
		});

		$('.slider-nav .slides').slick({
			    slidesToShow: 3,
			    slidesToScroll: 1,
			    asNavFor: '.slider-for .slides',
			    dots: false,
			    centerMode: true,
			    focusOnSelect: true,
			    arrows: false,
		});

		$('nav.mobile ul').addClass('hide');

		$('.menu-link').click(function(e) {
			e.preventDefault();
			if(!$('nav.mobile div > ul').is(':hidden')){
				$('nav.mobile div > ul').slideUp();
			} else {
				$('nav.mobile div > ul').slideDown();
			}
		});

		var $grid =	$('.grid').masonry({
		  itemSelector: '.grid-item',
		  columnWidth: '.grid-sizer',
		  percentPosition: true,
		});

		$grid.imagesLoaded().progress( function() {
		  $grid.masonry('layout');
		});

		var logo_nav_wrap_height = $('.logo-nav-wrap').outerHeight(true);
		var banner_height = $('.content-wrap').outerHeight(true);

		// $('.content-wrap').css('margin-top',logo_nav_wrap_height + 'px');
		$('.banner, .banner ul.background, .banner ul.background li, .banner ul.background div').css('min-height',banner_height+logo_nav_wrap_height);
		$('.single-portfolio article').css('padding-top',logo_nav_wrap_height);

		$(window).on('scroll',function() {
			if ($('nav.desktop').is(':visible')) {
				var y_scroll_pos = window.pageYOffset;
				if (y_scroll_pos > logo_nav_wrap_height) {
					$('.logo-nav-wrap').addClass('sticky');
				} else {
					$('.logo-nav-wrap').removeClass('sticky');
				}
			}
		});

		$('.portfolio-categories .mobile select').on('change', function() {
			$('.portfolio-item').addClass('active');
			var selected_category = (this.value);
			if (selected_category != 'all') {
				$('.portfolio-item').not('.' + selected_category).removeClass('active');
				$('.portfolio-item' + '.' + selected_category).addClass('active');
			} else {
				$('.portfolio-item').addClass('active');
			}
		});

		// Desktop
		$('.portfolio-categories .select ul li a').click(function(e){
			e.preventDefault();
			if ($(this).attr('id') != 'all') {
				if (!$(this).hasClass('active')) {
					$(this).addClass('active');
					$('.portfolio-categories #all').removeClass('active');
				} else { 
					$(this).removeClass('active');
				}
			} else {
				if (!$(this).hasClass('active')) {
					$('.portfolio-categories ul li a.active').removeClass('active'); 
					$(this).addClass('active');
				} else {
					//do nothing
				}
			}

			$('.portfolio-item').removeClass('active');

			$grid.imagesLoaded().progress( function() {
			  $grid.masonry('layout');
			});

			var selectedTypesArray = [];

			$('.portfolio-categories .active').each(function() {
				selectedTypesArray.push($(this).attr('id'));
			});

			$.each(selectedTypesArray, function(key, val) {
				if (val != 'all') {
					$('.portfolio-item.' + val).addClass('active');
					$grid.imagesLoaded().progress( function() {
					  $grid.masonry('layout');
					});
				} else {
					$('.portfolio-item').addClass('active');
					$grid.imagesLoaded().progress( function() {
					  $grid.masonry('layout');
					});
				}
			});
		});
	});

	//google maps

	/*
	*  new_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/

	function new_map( $el ) {
		
		// var
		var $markers = $el.find('.marker');
		
		
		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP,
			styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#000000"},{"weight":"1.05"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":"1.02"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"},{"hue":"#ff0000"},{"saturation":"-100"},{"lightness":"27"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"saturation":"-100"},{"lightness":"68"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"weight":"0.01"},{"saturation":"100"},{"lightness":"-21"},{"color":"#fccc9e"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"57"},{"gamma":"2.51"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"saturation":"-100"},{"lightness":"-32"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#f7f6f5"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#facaa3"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f3784f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#f3784f"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"},{"saturation":"-100"},{"lightness":"33"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#f3784f"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#ffdbb6"},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"},{"saturation":"-100"},{"lightness":"40"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#fbe9d4"},{"saturation":"-8"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#ffe7d5"},{"weight":"0.84"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f3784f"},{"visibility":"on"}]}] 
		};
		
		
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		
		
		// add a markers reference
		map.markers = [];
		
		
		// add markers
		$markers.each(function(){
			
	    	add_marker( $(this), map );
			
		});
		
		
		// center map
		center_map( map );
		
		
		// return
		return map;
		
	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );

			});
		}

	}

	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 14 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}

	}

	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	// global var
	var map = null;

	$(document).ready(function(){

		$('.acf-map').each(function(){

			// create map
			map = new_map( $(this) );

		});

	});

}(window.jQuery || window.$));