<?php

/**

Template Name: Our Team

/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<section>

				<div class="container">

					<div class="wysiwyg">

						<?php the_content( ); ?>

					</div>

				</div>

				</section>

				<div class="container">

					<div class="the-team">

					<?php if (have_rows('team_member')): ?>

						<ul>

						<?php while (have_rows('team_member')) : the_row(); ?>

							<?php $profile = get_sub_field('profile_picture'); ?>

							<li><div class="img-crop"><img src="<?php echo $profile['url']; ?>" alt="<?php echo $profile['alt']; ?>" /></div><h3><?php the_sub_field('name'); ?></h3><h4><?php the_sub_field('position'); ?></h4></li>

						<?php endwhile; ?>

						</ul>

					<?php endif; ?>

					</div>

				</div>

			</div>

		</article>

		<?php endwhile; endif; ?>

<?php get_footer(); ?>
