<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?>

		<div class="push"></div>

  	</div>

	<footer id="footer" class="source-org vcard" role="contentinfo">

		<?php if (!is_page('Contact')) { ?>

			<div class="footer-contact-wrap">

				<div class="container">

					<h2>Get a quote from us today!</h2>

					<p>Tell us a little it about your business and we'll come back to you real soon with an estimate.</p>

					<?php echo do_shortcode( '[contact-form-7 id="179" title="Contact form 1"]' ); ?>

				</div>

			</div>

		<?php } ?>

			<div class="menu-wrap">

				<div class="container">

					<?php wp_nav_menu( array('theme_location' => 'footer') ); ?>

				</div>

			</div>

		</div>

	</footer>

	<div class="copyright">

		<div class="container">

			&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?>

		</div>

	</div>

	<?php wp_footer(); ?>

</body>

</html>
