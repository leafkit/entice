<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

 	<article class="post" id="post-<?php the_ID(); ?>">

 		<div class="entry">

		 	<section class="services">
		 		<div class="container">
					<h2>Sorry, it looks like you're lost.</h2>
					<a href="<?php site_url(); ?>" class="trailing-link">Return Home</a>
				</div>
		 	</section>

		 </div>

	</article>


<?php get_footer(); ?>