<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<?php if (have_rows('layouts')): 

						while (have_rows('layouts')): the_row(); ?>

						<?php 

						if (get_sub_field('background_image')) {

							$background_image = get_sub_field('background_image');
							$style = 'style="background-image:url('.$background_image['url'].');color:#fff;"';

						} elseif (get_sub_field('background_colour')) {

							$background_colour = get_sub_field('background_colour');
							$style = ' style="background-color:'.$background_colour.';"';

						} else {

							$style = '';

						}


						?>

						<section class="<?php echo get_row_layout(); ?>"<?php echo $style; ?> >
							
							<?php if (get_row_layout() == 'padded_content'): ?>

								
								<?php

								$alignment = '';

								$featured_image = get_sub_field('featured_image');
								if ($featured_image) {
									$alignment = get_sub_field('image_alignment');
									$alignment = ' '.$alignment;
								}

								 ?>

								<div class="container<?php echo $alignment; ?>">									

									<?php if ($featured_image) { ?>
									<div class="featured-image">
										<img src="<?php echo $featured_image['url']; ?>" alt="<?php echo $featured_image['alt']; ?>" />
									</div>
									<?php } ?>

									<div class="wysiwyg">			

										<?php the_sub_field('wysiwyg'); ?>

									</div>

									<?php 

									$post_object = get_sub_field('trailing_link');

									if ($post_object) {

										$post = $post_object;
										setup_postdata( $post ); ?>

										<div class="link"><a href="<?php the_permalink(); ?>" class="trailing-link"><?php the_sub_field('trailing_link_text') ?></a></div>

									<?php } wp_reset_postdata(); ?>

								</div>

							<?php elseif (get_row_layout() == 'portfolio'): ?>

							<h2><?php the_sub_field('title'); ?></h2>

							<?php if (get_sub_field('show_portfolio_categories') == 'Yes') { ?>

							<?php 

								$taxonomy = 'portfolio-category';

								$terms = get_terms([
								    'taxonomy' => $taxonomy,
								    'hide_empty' => false,
								]);

							?>

							<div class="portfolio-categories">

								<div class="mobile select">

									<select>

										<option value="all">All</option>

										<?php

										foreach ($terms as $type => $value) {
											echo '<option value="'.$value->slug.'">'.$value->name.'</option>';
										}

										?>

									</select>

								</div>

								<div class="desktop select">

									<ul>

										<!-- <li><a href="#" id="all" class="active">All</a></li> -->

									<?php

									foreach ($terms as $type => $value) {
											//echo '<li><a href="#" id="'.$value->slug.'" >'.$value->name.'</a></li>';
									}

									?>

										<li><p>To view more of our recent work, go to <a href="/portfolio/" class="portfolio-link desktop">our portfolio</a></p></li>

									</ul>

								</div>

							</div>

							<?php } ?>

							<?php

								$all_or_selection = get_sub_field('all_or_selection');

								if ($all_or_selection == 'All') {

									$args = array(
										'posts_per_page' => get_sub_field('number_of_items'),
										'orderby' => get_sub_field('order_by'),
										'order' => get_sub_field('order'),
										'post_type' => 'portfolio'
									);

									$posts_array = get_posts($args);

								} else if ($all_or_selection == 'Category') {

									$category = get_sub_field('category');

									$args = array(
										'posts_per_page' => get_sub_field('number_of_items'),
										'orderby' => get_sub_field('order_by'),
										'order' => get_sub_field('order'),
										'post_type' => 'portfolio',
										'portfolio-category' => $category->name
									);

									$posts_array = get_posts($args);

								} else {

									$posts_array = get_sub_field('portfolio_selections');

								}

								$count = count($posts_array);

								if ($count <= 4 ) {

									$width = 'portfolio-items-'.$count;

								} else {

									$width = 'portfolio-items-4';

								}

								?>

								<div class="portfolio-wrap <?php echo $width; ?>">

								<ul class="grid">
								<div class="grid-sizer"></div>

								<?php

								$i = 0;

								foreach ($posts_array as $post) {

									$portfolio_types = '';
									$portfolio_types = array();

									$types = get_the_terms( $post->ID, 'portfolio-category' );

									$array = json_decode(json_encode($types), true);

									if ($array) {

										foreach ($array as $type => $value) {
											$portfolio_types[] = $value['slug'];
										}

										$str = implode(' ', $portfolio_types);

									}

								setup_postdata($post);
								$img = get_the_post_thumbnail( $post->ID, 'full');

								?><!----><li class="portfolio-item grid-item <?php echo $str; ?> active">

										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

											<div class="img-wrap">

												<?php echo $img; ?>

												<div class="excerpt">

													<?php $information= get_field('excerpt');

													$excerpt = substr($information, 0, 140);

													echo '<p>'.$excerpt.'...</p>';

													?>

													<span>Read More</span>

												</div>

											</div>

										</a>

									</li><!----><?php 

									//$i++; if ($i % 4 == 0) { echo '</ul><ul>'; } 

									} wp_reset_postdata(); ?>

								</ul>

							</div>

							<div class="link mobile"><a href="/portfolio/" class="trailing-link">View Portfolio</a></div>

							<?php elseif (get_row_layout() == 'testimonials'): ?>

								<div class="container">

									<div class="testimonials-wrap">

										<ul class="slides">

										<?php $testimonials = get_sub_field('testimonial');

										foreach ($testimonials as $post) {

											setup_postdata( $post ); ?>

											<?php $image = get_the_post_thumbnail_url( $post, 'full' ); ?>

											<li>

												<!-- <div class="overlay"></div> -->

													<div class="info-wrap">

														<div class="quote">

															<h3>"<?php the_field('quote'); ?>"</h3>
																
														</div>

														<div class="author">

															<h3><?php the_field('author'); ?> | <strong><?php the_field('position'); ?></strong></h3>

														</div>

													</div>

													<div class="img-wrap">

														<img src="<?php echo $image; ?>" />

													</div>

											</li>

										<?php wp_reset_postdata(); } ?>

										</ul>

									</div>

								</div>

							<?php elseif (get_row_layout() == 'contact_section') : ?>

								<div class="container">

									<div class="contact-wrap">
										<div class="wysiwyg">										
											<?php the_sub_field('wysiwyg'); ?>
										</div>
										<div>
											<?php echo do_shortcode( '[contact-form-7 id="179" title="Contact form 1"]' ); ?>
										</div>
									</div>
									
								</div>

								<?php 

								$location = get_field('address','option');

								if( !empty($location) ):
								?>
								<div class="acf-map">
									<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
								</div>
								<?php endif; ?>

							<?php elseif (get_row_layout() == 'our_clients') : ?>

								<div class="container">

									<h2><?php the_sub_field('heading'); ?></h2>

									<h6><?php the_sub_field('sub_heading'); ?></h6>

									<?php if (have_rows('client_logos','option')) { ?>

										<ul>

										<?php while (have_rows('client_logos','option')) : the_row(); ?>

											<?php $logo = get_sub_field('image'); ?>

											<li><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></li>

										<?php endwhile; ?>

										</ul>

									<?php } ?>

								</div>

							<?php elseif (get_row_layout() == 'wide_image_with_content') : ?>

								<?php

								$alignment = '';
								if (get_sub_field('image_alignment') == 'Left') {
									$alignment = ' Left';
								} elseif (get_sub_field('image_alignment') == 'Right') {
									$alignment = ' Right';
								}
								?>

								<div class="container<?php echo $alignment; ?><?php if (get_sub_field('background_image')) {echo ' has_bg';} ?>">

									<div class="featured-image<?php 
									if (get_sub_field('feature_image_shadow') == 'Dark') {echo ' dark';} elseif (get_sub_field('feature_image_shadow') == 'Light') {echo ' light';} ?>">

										<?php 
										$wide_image = get_sub_field('featured_image');
										$trailing_link = get_sub_field('trailing_link'); ?>

										<img src="<?php echo $wide_image['url']; ?>" alt="<?php echo $wide_image['alt']; ?>" />

									</div>

									<div class="wysiwyg">

										<?php the_sub_field('wysiwyg'); 								

									$post_object = get_sub_field('trailing_link');

									if ($post_object) {

										$post = $post_object;
										setup_postdata( $post ); ?>

										<div class="link"><a href="<?php the_permalink(); ?>" class="trailing-link"><?php the_sub_field('trailing_link_text') ?></a></div>

									<?php } wp_reset_postdata(); ?>

									</div>

								</div>

							<?php elseif (get_row_layout() == 'services') : ?>

								<div class="container">

									<h2><?php the_sub_field('services_title'); ?></h2>

									<?php if (have_rows('service_selection')) { ?>

									<ul>

										<?php while(have_rows('service_selection')): the_row(); ?>
											<?php $service_icon = get_sub_field('icon'); ?>
											<li class="service">
												<img src="<?php echo $service_icon['url']; ?>" alt="<?php echo $service_icon['alt']; ?>">
												<h4><?php the_sub_field('title'); ?></h4>
												<p><?php the_sub_field('description'); ?></p>
												<a href="<?php the_sub_field('link'); ?>">Learn More <i class="fa fa-arrow-right"></i></a>
											</li>
										<?php endwhile; ?>

									</ul>

									<?php } ?>

								</div>

							<?php endif; //end of get_row_layouts ?>

						</section>

						<?php endwhile;

					endif;

				?>

				<?php

				$page = get_page_by_title( 'Services', $output = OBJECT, $post_type = 'page' );

				if (!is_page('Services') && is_page_child($page->ID)) { ?>
					<section class="services_link">
						<div class="container">
							To view other areas where we can help your business, see <a href="<?php the_permalink( $page->ID ); ?>">our services</a>
						</div>
					</section>

					<section class="testimonials">

						<div class="container">

							<div class="testimonials-wrap">

								<ul class="slides">

								<?php 

								$args = array(
									'post_type' => 'testimonial' 
								);

								$testimonials = get_posts($args);

								foreach ($testimonials as $post) {

									setup_postdata( $post ); ?>

									<?php $image = get_the_post_thumbnail_url( $post, 'full' ); ?>

									<li>

										<!-- <div class="overlay"></div> -->

											<div class="info-wrap">

												<div class="quote">

													<h3>"<?php the_field('quote'); ?>"</h3>
														
												</div>

												<div class="author">

													<h3><?php the_field('author'); ?> | <strong><?php the_field('position'); ?></strong></h3>

												</div>

											</div>

											<div class="img-wrap">

												<img src="<?php echo $image; ?>" />

											</div>

									</li>

								<?php wp_reset_postdata(); } ?>

								</ul>

							</div>

						</div>

					</section>

					<section class="our_clients">
						<div class="container">

							<h2>Our clients</h2>

							<h6>Some clients we have worked with - </h6>

							<?php if (have_rows('client_logos','option')) { ?>

								<ul>

								<?php while (have_rows('client_logos','option')) : the_row(); ?>

									<?php $logo = get_sub_field('image'); ?>

									<li><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></li>

								<?php endwhile; ?>

								</ul>

							<?php } ?>

						</div>						
					</section>

				<?php } ?>

			</div>

		</article>

		<?php endwhile; endif; ?>

<?php get_footer(); ?>
