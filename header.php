<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?><!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head data-template-set="html5-reset-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name=”mobile-web-app-capable” content=”yes”>

	
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/jpg" href="/favicon.ico">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/ie8-and-down.css" />
	<![endif]-->

	<!-- Lea Verou's Prefix Free, lets you use only un-prefixed properties in yuor CSS files -->
    <!-- <script src="<?php echo get_template_directory_uri(); ?>/_/js/prefixfree.min.js"></script> -->

	<!-- This is an un-minified, complete version of Modernizr.
		 Before you move to production, you should generate a custom build that only has the detects you need. -->
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr-2.8.0.dev.js"></script>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div id="wrapper">

		<div class="header-wrap">

				<div class="logo-nav-wrap">

					<div class="container">
						<header id="header" role="banner">
							<?php $image = get_field('logo','option'); ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
						</header>
						<nav id="nav" role="navigation" class="desktop">
							<?php wp_nav_menu( array('theme_location' => 'primary') ); ?>
						</nav>
						<div class="phone-wrap">
							<strong><i class="fa fa-phone" aria-hidden="true"></i><a href="<?php strip_the_phone(); ?>"><?php the_field('phone','option'); ?></a></strong>
						</div>
						<nav id="nav" role="navigation" class="mobile">
							<i class="fa fa-bars menu-link" aria-hidden="true"></i>
							<?php wp_nav_menu( array('theme_location' => 'primary') ); ?>
						</nav>
					</div>
				</div>
				<div class="banner">
				<?php
				if (is_front_page()) {
					if( have_rows('banner','option') ) { ?>
						<ul class="slides background">
					 	<?php while ( have_rows('banner','option') ) : the_row(); 
					 		$background_image = get_sub_field('banner_background');?>
					        <li style="background-image: url(<?php echo $background_image['url']; ?>)"></li>
					    <?php endwhile;
					} else {
						// if not home page, don't show background slides in the header
					}
				} else { ?>
					<ul class="slides background">
					<?php if (is_home() && get_option('page_for_posts') ) {
					    $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'full'); 
					    $background_image = $img[0];
					} else if (is_single()) {
						if (get_field('banner_image')) {
							$background_image = get_field('banner_image')['url'];
						} else {
							$background_image = get_field('default_header','option')['url'];
						}
					} else if (is_404()) {
						$background_image = get_field('default_header','option')['url'];
					} else if (has_post_thumbnail()) {
						$background_image = get_the_post_thumbnail_url();
					} else {
						$background_image = get_field('default_header','option');
						$background_image = $background_image['url']; 
					} ?>

			        <li style="background-image: url(<?php echo $background_image; ?>)"></li>

				<?php } ?>

					</ul>

					<div class="content-wrap">

					<?php

					if (is_front_page()) :

						if( have_rows('banner','option') ): ?>

							<ul class="slides foreground">

						 	<?php while ( have_rows('banner','option') ) : the_row(); 

						 		$foreground_image = get_sub_field('banner_foreground'); ?>

						        <li><img src="<?php echo $foreground_image['url']; ?>"></li>

						    <?php endwhile;

						else :

						endif;

					else :

					endif;

					?>

				</ul>

				<?php 
				// Content section of homepage banner. Found under Theme Settings > Header > "Banner Content" tab
				if (is_front_page()) { ?>

					<div><?php the_field('banner_content','option'); ?>
						
						<a class="banner-cta" href="<?php the_field('call_to_action_link','option'); ?>"><?php the_field('call_to_action_text','option'); ?></a>

					</div>

				<?php } elseif (is_home()) { ?>

					<div>

						<h1>Entice Blog</h1>

					</div>

				<?php } elseif (is_404()) { ?>

					<div>

						<h1><?php _e('Error 404 - Page Not Found','html5reset'); ?></h1>

					</div>

				<?php } else { ?>

					<div>

						<h1 class="page-title"><?php the_title(); ?></h1>

						<?php if (get_field('sub_heading')) { ?>
							<h6><?php the_field('sub_heading'); ?></h6>
						<?php } ?>

					</div>

				<?php } ?>

				</div>

			</div>

		</div>

