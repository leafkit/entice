<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header('portfolio'); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<div class="entry-content">

				<div class="gallery-wrap">

					<?php $gallery = get_field('gallery');

					if ($gallery) { ?>

					<div class="portfolio-featured-image slider-for">
						
						<ul class="slides">

							<?php foreach ($gallery as $item) { ?>			

								<li><img src="<?php echo $item['url']; ?>"></li>

							<?php } ?>

						</ul>

					</div>

					<div class="portfolio-item-gallery slider-nav">

						<ul class="slides">

							<?php foreach ($gallery as $item) { ?>			

								<li><img src="<?php echo $item['url']; ?>"></li>

							<?php } ?>

						</ul>

					</div>

					<?php } ?>

				</div>

				<div class="info">

					<h1 class="entry-title"><?php the_title(); ?></h1>
					
					<?php the_field('information'); ?>

					<div><a class="portfolio-website-link" href="<?php the_field('link'); ?>">View Website</a></div>

				</div>

			</div>
			
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>